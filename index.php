<?php

include __DIR__ . "/vendor/autoload.php";

$app = new \app\App();
try {
    $app->run();
} catch (Exception $e) {
    http_response_code (404);
    echo $e->getMessage();
}
