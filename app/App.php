<?php


namespace app;


use models\Board;
use models\CSMBBoard;
use models\CSMBoard;
use models\JsonFormatter;
use models\Student;
use models\XmlFormatter;

class App
{
    /**
     * @throws \Exception
     */
    public function run() {

        if (!isset($_GET['student'])) {
            throw new \Exception('incorrect request data');
        }
        if (!$student = Student::find($_GET['student'])) {
            throw new \Exception('student not found');
        }

        switch ($student->getBoardType()) {
            case Board::TYPE_CSM:
                $board = new CSMBoard($student, new JsonFormatter());
                break;
            case Board::TYPE_CSMB:
                $board = new CSMBBoard($student, new XmlFormatter());
                break;
            default:
                throw new \Exception('incorrect board type');
        }
        header($board->getDataFormatter()->getHeaders());
        echo $board->exportData();
    }
}