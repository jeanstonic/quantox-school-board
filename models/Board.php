<?php


namespace models;


abstract class Board
{
    const TYPE_CSM = "CSM";
    const TYPE_CSMB = "CSMB";

    /**
     * @var Student
     */
    protected $student;
    /**
     * @var DataFormatter
     */
    protected $dataFormatter;

    public function __construct($student, $dataFormatter)
    {
        $this->student = $student;
        $this->dataFormatter = $dataFormatter;
    }

    /**
     * @return mixed
     */
    abstract public function getAvg();

    /**
     * @return mixed
     */
    abstract public function getPass();

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->student->getId(),
            'name' => $this->student->getName(),
            'grades' => $this->student->getGrades(),
            'average' => $this->getAvg(),
            'passed' => $this->getPass() ? "Pass" : "Fail",
        ];
    }

    /**
     * @return DataFormatter
     */
    public function getDataFormatter()
    {
        return $this->dataFormatter;
    }

    /**
     * @return string
     */
    public function exportData()
    {
        $this->dataFormatter->setData($this->toArray());
        $this->dataFormatter->setDataName('student_grades');
        return $this->dataFormatter->export();
    }
}