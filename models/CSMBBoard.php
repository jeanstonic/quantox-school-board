<?php


namespace models;


class CSMBBoard extends Board
{
    protected $minGrade = 8;

    public function getAvg()
    {
        if (count($this->student->getGrades()) == 1) {
            return $this->student->getGrades()[0];
        } else {
            $grades = $this->student->getGrades();
            sort($grades);
            array_shift($grades);
            return array_sum($grades) / count($grades);
        }
    }

    public function getPass()
    {
        return $this->getAvg() > $this->minGrade;
    }
}