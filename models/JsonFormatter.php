<?php


namespace models;


class JsonFormatter extends DataFormatter
{

    /**
     * @param $data
     * @return string
     */
    public function export()
    {
        return json_encode($this->data);
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return 'Content-type: application/json';
    }
}