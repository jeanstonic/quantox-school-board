<?php


namespace models;


use PDO;

class DbQuery
{
    public $pdo;

    public function __construct()
    {
        $host = '127.0.0.1';
        $db   = 'students_dashboard';
        $user = 'root';
        $pass = '';
        $charset = 'utf8mb4';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $user, $pass, $opt);
    }

    public function findStudentById($id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM students WHERE id = :id LIMIT 1');
        $stmt->execute(array('id' => $id));

        $row = $stmt->fetch();

        var_dump($row);

    }
}