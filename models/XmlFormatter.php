<?php


namespace models;


class XmlFormatter extends DataFormatter
{
    /**
     * @param $data
     * @return string
     */
    public function export()
    {
        $xml = new \SimpleXMLElement("<{$this->dataName}></{$this->dataName}>");
        foreach ($this->data as $k => $v) {
            if (is_array($v)) {
                $child = $xml->addChild($k);
                $this->addArray($child, $v);
            } else {
                $xml->addChild($k, $v);
            }
        }
        return $xml->asXML();
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return 'Content-type: text/xml';
    }

    /**
     * @param \SimpleXMLElement $nod
     * @param $array
     */
    protected function addArray(&$nod, $array)
    {
        foreach ($array as $key => $element) {
            if (is_int($key)) {
                $key = "value";
            }
            if (is_array($element)) {
                $child = $nod->addChild($key);
                $this->addArray($child, $element);
            }
            $nod->addChild($key, $element);
        }
    }
}