<?php


namespace models;


use Exception;

class Student
{
    private $name;
    private $id;
    private $grades;
    private $boardType;
    /**
     * @var Board
     */
    private $board = null;

    /**
     * Student constructor.
     * @param int $id
     * @param string $name
     * @param int[] $grades
     * @param string $boardType
     * @throws Exception
     */
    public function __construct($id, $name, $grades, $boardType)
    {
        $this->id = $id;
        $this->name = $name;
        $this->grades = $grades;
        $this->boardType = $boardType;
    }

    /**
     * @param $id
     * @return Student|null
     * @throws Exception
     */
    public static function find($id)
    {
        $query = new DbQuery();

        $stmt = $query->pdo->prepare('SELECT * FROM students WHERE id = :id LIMIT 1');
        $stmt->execute(array('id' => $id));
        $student = $stmt->fetch();
        if (!$student) return null;

        $stmt = $query->pdo->prepare(
            'SELECT s.id, s.name, s.dashboard, sg.grade
                        FROM students s 
                        LEFT JOIN students_grades sg ON sg.student_id = s.id
                        WHERE id = :id'
        );
        $stmt->execute(array('id' => $id));

        $student = $stmt->fetch();
        if (!$student) return null;

        $grades = [];
        $name = $student['name'];
        $dashboardType = $student['dashboard'];
        $grades[] = $student['grade'];
        while ($row = $stmt->fetch())
        {
            $grades[] = $row['grade'];
        }

        return new Student($id, $name, $grades, $dashboardType);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int[]
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * @return string
     */
    public function getBoardType()
    {
        return $this->boardType;
    }
}