<?php


namespace models;


abstract class DataFormatter
{
    /**
     * @var array
     */
    protected $data;
    protected $dataName = 'data';

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $dataName
     */
    public function setDataName($dataName)
    {
        $this->dataName = $dataName;
    }

    /**
     * @return string
     */
    abstract public function export();

    /**
     * @return string
     */
    abstract public function getHeaders();
}