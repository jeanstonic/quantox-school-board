<?php


namespace models;


class CSMBoard extends Board
{
    protected $minGrade = 7;

    public function getAvg()
    {
        return array_sum($this->student->getGrades()) / count($this->student->getGrades());
    }

    public function getPass()
    {
        return $this->getAvg() >= $this->minGrade;
    }
}